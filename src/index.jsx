import React from 'react';
import ReactDOM from 'react-dom';

import Glossary from './components/Glossary';

const groups = [
  {
    id: 1,
    description: "qwerty",
    terms: [
      {
        id: 1,
        title: "qwe"
      },
      {
        id: 2,
        title: "rty"
      }
    ]
  },
  {
    id: 2,
    description: "asdfgh",
    terms: [
      {
        id: 3,
        title: "asd"
      },
      {
        id: 4,
        title: "fgh"
      }
    ]
  }
]

ReactDOM.render(
  <Glossary groups={groups} />,
  document.getElementById("glossary")
);
