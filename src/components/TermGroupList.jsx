import React, { Component } from 'react';

import TermGroupRow from './TermGroupRow';

export default class TermGroupList extends Component {
  render() {
    return (
      <table>
        <tbody>{this.props.groups.map(group =>
          <TermGroupRow group={group} />)}
        </tbody>
      </table>
    );
  }
}
