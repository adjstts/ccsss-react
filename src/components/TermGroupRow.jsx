import React, { Component } from 'react';

import TermList from './TermList';
import GroupDescription from './GroupDescription';
import GroupActions from './GroupActions';

import TermListEdit from './TermListEdit';
import GroupDescriptionEdit from './GroupDescriptionEdit';
import GroupActionsEdit from './GroupActionsEdit';

export default class TermGroupRow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isInEditState: false
    };
  }

  render() {
    if (this.state.isInEditState)
      return (
        <tr>
          <td><TermListEdit terms={this.props.group.terms} /></td>
          <td><GroupDescriptionEdit description={this.props.group.description} /></td>
          <td><GroupActionsEdit /></td>    
        </tr>
      );

    return (
      <tr>  
        <td><TermList terms={this.props.group.terms} /></td>
        <td><GroupDescription description={this.props.group.description} /></td>
        <td><GroupActions /></td>
      </tr>
    );  
  }
}
