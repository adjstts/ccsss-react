import React, { Component } from 'react';

export default class TermList extends Component {
  render() {
    return (this.props.terms.map(term => term.title).join(","));
  }
}
