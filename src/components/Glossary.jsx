import React, { Component } from 'react';

import SearchBar from './SearchBar';
import TermGroupList from './TermGroupList';

export default class Glossary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchString: ""
    };
  }

  render() {
    return (
      <div>
        <h2>Глоссарий</h2>
        <hr/>
        <SearchBar searchString={this.state.searchString}/>
        <TermGroupList searchString={this.state.searchString} groups={this.props.groups} />
      </div>
    );
  }
}
