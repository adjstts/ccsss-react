import React, { Component } from 'react';

export default class GroupDescriptionEdit extends Component {
  render() {
    return (
      <textarea>{this.props.description}</textarea>
    );
  }
}
