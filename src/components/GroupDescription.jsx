import React, { Component } from 'react';

export default class GroupDescription extends Component {
  render() {
    return (
      <p>{this.props.description}</p>
    );
  }
}
