import React, { Component } from 'react';

export default class SearchBar extends Component {
  render() {
    return (
      <input type="search" value={this.props.searchString} />
    );
  }
}
